% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

\documentclass[12pt]{IJM-article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{mathtools}
%\usepackage{mathptmx}
\usepackage{amsmath}
\usepackage{xspace}
\usepackage[titletoc]{appendix}
\usepackage[official]{eurosym}
\usepackage{tabularx}
\usepackage{multirow}
%\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{adjustbox}
\usepackage{longtable}
\usepackage{rotating}
\usepackage{setspace}
\usepackage{booktabs}
\usepackage[abs]{overpic}


\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000
\sloppy
\raggedbottom%

\begin{document}

\title{Constructing a Synthetic City for Estimating Spatially Disaggregated
    Heat Demand}

%% Author 1
\author[Munoz]{M. Esteban Mu\~noz H.}
\address{Technical Urban Infrastructure Systems Group, HafenCity University\\ Hamburg, Germany}
\email{marcelo.hidalgo@hcu-hamburg.de}

%% Author 2
\author[Dochev]{Ivan Dochev}
\address{Technical Urban Infrastructure Systems Group, HafenCity University\\ Hamburg, Germany}
\email{ivan.dochev@hcu-hamburg.de}

%% Author 3
\author[Seller]{Hannes Seller}
\address{Technical Urban Infrastructure Systems Group, HafenCity University\\ Hamburg, Germany}
\email{hannes.seller@hcu-hamburg.de}

%% Author 4
\author[Peters]{Irene Peters}
\address{Technical Urban Infrastructure Systems Group, HafenCity University\\ Hamburg, Germany}
\email{irene.peters@hcu-hamburg.de}

\keywords{Heating demand,
          synthetic building stock,
          spatial microsimulation,
          GREGWT}

\JELclassification{J11, Q40, C15, C63}
\issue{-}{-}{-}{-}

\maketitle

\begin{abstract}
We present a procedure for creating a spatially referenced building stock with
population living therein ``a synthetic city'' for the case of Germany.
The level of spatial disaggregation is the European NUTS--3 level for which data
from the newest census (2011) exist.
Our application is on the
estimation of heat demand.

Using the German microcensus (2010), which contains both: (a) detailed
sociodemographic characteristics of individuals and (b) detailed information on the
type of buildings in which these individuals live. With this data we can generate
not only a synthetic population but also a synthetic
building stock. The microcensus records the construction year and number of
dwelling units of buildings.
This allow us to classify the buildings
for the estimation of heat demand.

This procedure has two major advantages:
(1) there exist many models
for the estimation of heat demand at building level, we can make use of these
models,
and (2)
with the microcensus as the only
required data source we are able to estimate heat demand at a spatially
disaggregated level for the entire country.

We conclude our paper with an internal validation of the microsimulation model
by means of the Total Absolute Error $TAE$ and present the first results from
this model aggregated at the NUTS--3 level for the entire country. We briefly
discuss the observed patters of the results and attempt to hypothesize on the
reasons behind this patterns. We also discuss the difficulties of an external
validation of this model and how we can address them in the future.
\end{abstract}

\section{Introduction: Creating a Synthetic Building Stock}\label{sec:intro}

The development of a synthetic city representing the urban fabric of urban
agglomerations has proved helpful for the development of urban
models, the focus of this endeavour has been the use of remote sensing data or
other type of image and laser data available at a low aggregation level in
order to generate urban structures.
\citeA{Laycock.2003} presents an overview of used data sources and methods for
the generation of urban structures.
\citeA{Parish.2001} used a procedural approach based on L-systems to generate
urban structures. The authors use different maps as input for the generation of
the geometrical representation of the city. Our aim is to expand these methods
by creating appropriate data regarding the building stock and the population
living and working on this building stock that can be used as input to these
models for the generation of synthetic cities.
\citeA{Kang.2012} create a set of synthetic cities in order to asses the
relationship between urban morphologies and human mobility.
\citeA{Kii.2014} develop a simple synthetic city for the assessment of urban
transport policies, the authors generate the synthetic city and use this data
as input for a land use model, subsequently the authors apply the postulated
policies to the model in order to asses them. The authors argue in favor of
including user behaviour in urban models.
\citeA{Farber.2013a} generate eighty different synthetic cities in order to
analyze the Social Interaction Potential of these environments based on
commuting patterns and land use distribution.
\citeA{Bagchi.2013} use a synthetic city for the simulation of fire dispersion
on an electrical distribution grid, in this case the authors represent the
building stock and an electrical grid.
\citeA{Mei.2015} develop a synthetic city
in order to study the diffusion of infectious diseases. The authors use the
synthetic city in order to understand the outbreak of influenza in dense
populated urban areas in China. In this case the authors do not require a
detailed description of the building stock geometry but only the building use.
\citeA{Stoetzer.2015} define a simple synthetic city for the estimation of the
potential load shift of commercial and residential electricity demand. Because
the authors are only interested in electric consumption, they do not create a
geometrical representation of the building stock nor do they have any type of
geo-reference in the synthetic city. In this paper the authors define the
electric consumption as a function of household size.
Many of the examples presented above could be implemented on a more realistic
environment describing the characteristics of the building stock and the
population living on them.

The development of a robust method for the creation of synthetic cities
representing specific urban agglomerations with known population aggregates
constitutes the scope of this paper. This paper presents first results from
this endeavour. It is shown the developed method for the representation of
a synthetic city, describing individuals and the characteristics of their
households and the building they reside on.
Here we do not discuss in detail the pursued method for the
representation of geo-referenced geometrical objects. We present some
discussion regarding the different alternatives to generate a geometrical
representation of the synthetic city extending the methods described in this
paper.

The method used in this analysis is a spatial microsimulation method.
Microsimulation, introduced by~\citeA{Orcutt.1977} is a commonly used method
among social scientist used to simulate a large range of social phenomena at a
micro-level. The first step of this method is normally the generation of a
synthetic population representing the population under analysis. The spatial
microsimulation methodology extends this concept by allocating estimated
synthetic populations to geographical areas~\cite{Clarke.1987}.
This simulation method is applied by a large number of disciplines.
\citeA{Brown.2002} and~\citeA{Smith.2011} argue the use of this type of models
for the analysis of health systems, the modeling of resources consumption is
addressed by \citeA{Williamson.1996,Williamson.2002} for the estimation of
water consumption and \citeA{Chingcuanco.2012} and \citeA{MunozH.2014.IJM} for
the estimation of energy demand.
Many transport models use this approach for the generation of synthetic
populations~\cite{Farooq.2013}.
For overview of spatial microsimulation models, its applications and methods
see~\cite{Tanton.2014,ODonoghue.2014}.
In this paper we make use of an ``generalised regression weighting'' (GREGWT) algorithm to create a synthetic
population. We use the available R library~\cite{MunozH.2015.GREGWTR},
implementing the GREGWT algorithm, originally developed by the Australian
Bureau of Statistics (ABS)~\cite{Bell.2000}. This algorithm is used by
the National Center for Social and Economic Modeling (NATSEM) on their spatial
microsimulation model spatialMSM~\cite{Tanton.2007}.

The presented paper is structured as follows: Section~\ref{sec:data} presents
and describes the used data for the analysis and the undertaken steps in
preparing the data for the simulation, the next section,
Section~\ref{sec:heat}, describes the computation method to estimate heat
demand for each individual on the microcensus and the assumptions made for this
computation.
Section~\ref{sec:gregwt} describes the method used for the reweighting of the
microcensus with the computed heating demand values. We benchmark the survey to
benchmarks describing individuals, dwelling units and buildings. These process
is described under Section~\ref{sec:bench}.
The results from the simulation are described and discussed under
Section~\ref{sec:results}.
On Section~\ref{sec:next_steps} we highlight the benefits and shortcomings of
the developed method and propose extensions to this method in order to address
some shortcomings of the applied method.
In the last section, Section~\ref{sec:conclusions}, we make a brief resume of
the paper, its main findings and possible applications for the infrastructure
planning of urban environments.

\section{Data: Census (2011) \& Micro Census (2010)}\label{sec:data}

In the literature we find two type of models for the construction of synthetic
data: (1) synthetic reconstruction and (2) reweighting~\cite{Rahman.2008}.
The GREGWT algorithm forms part of the latter group.
This algorithm is a deterministic method that reweights a sample survey to
match aggregated statistics available at small geographical areas.

In order to create a synthetic population with the GREGWT method we need
two data sets: (1) a survey sample containing individual records and
(2) aggregated statistics available at a NUTS--3 geographical level.  For the purpose of this
simulation we want to create a synthetic population describing the demographic
characteristics of the individuals as well as characteristics of the dwelling units
these individuals reside on.
In order to generate such a data set we select data describing three different
aggregation units: (1) individuals, (2) household/dwelling units and (3)
buildings. Table~\ref{tab:census} lists the used variables from the two data
sets.

\input{TABLES/benchmarks}% \ref{tab:census}

The synthetic population is represented as the reweighted German micro census,
we reweight this survey with help of an R library implementing the GREGWT
method~\cite{MunozH.2015.GREGWTR}. The GREGWT method is classified as a
deterministic reweight method~\cite{Tanton.2014a}.
Deterministic reweighting methods aim to reweight a survey to match known
aggregated values of geographical areas. The sample size and availability of the data of these
geographical areas vary between countries. For the European Union a standard
incorporating the different national definitions exist. This is the
Nomenclature of Territorial Units for Statistics
(NUTS\footnote{\url{http://ec.europa.eu/eurostat/web/nuts/overview}})
standard. This nomenclature describes four hierarchies: (0) national
territories; (1) NUTS--1; (2) NUTS--2; and NUTS--3. A reweighting of a national
survey could be implemented at any of the NUTS levels. Depending on the
research question a suitable geographical area should be selected.
These geographical areas have different names on each country and the authors
referee them according to the use case location. These areas are known as:
(a) Summary Files in the U.S.; (b) Profile Tables or Basic Summary
Tabulations (BSTs) in Canada; (c) and Small Area Statistics in the
U.K.~\cite{Pritchard.2012}.

\section{Heating Demand}\label{sec:heat}

The heating demand is computed for each individual in the microcensus.
For the computation of heating demand of each individual we need to
consider the characteristics of the building stock.
In order to take these characteristics into account we make use of the IWU
building typology~\cite{Diefenbach.2010b, Loga.2011}.
We classify each individual to one of the 36 IWU building typology types.
For the estimation of heating demand per dwelling unit we need to take into
account the household size and divide the estimated heating demand by its
household size.
For a household with four members we compute the heating demand for each member
of the family as $kWh$ per dwelling unit $m^2$.
In order to get the heating demand of the dwelling unit we sum the heating
demand of each member divided by household size.
On this model the computed heating demand of each family member is the same,
nonetheless if we where to take user behaviour into account this values would
differ from each other~\cite{MunozH.2014.IBPSA-JP}.
This is necessary because the computed heating demand is the estimated heating
demand per squared meter of dwelling unit.
An alternative to this would be to divide the dwelling unit area by household
size.

In order to classify the microcensus into the building types we use three
parameters from the microcensus:
(1) building construction year,
(2) dwelling unit size, and
(3) number of dwelling units per building.
A summary of the rules applied to the microcensus to achieve this
classification are listed in Table~\ref{tab:rules}.

The main parameter used for the classification is the building construction
year.
We use the number of dwelling units to differentiate between single
family houses and multi-family houses.
Finally, the dwelling unit size multiplied by the number of dwelling units is
used to distinguish between small multi-family housed, large multi-family
houses and high rise buildings.
The specific heating values of the IWU building typology used in this analysis
are listed in Table~\ref{tab:IWU-de}.

\input{TABLES/IWU}

The advantage of using a building typology for the estimation of heating demand is
that we don't need to take assumptions about the building geometry.
This is because the values listed under the building typology represent
specific heating demand.
In order to compute the absolute heating demand we simply multiply this value
by the building square meters.
In order to explicitly account for building geometry we need to allocate
individuals to a digital cadastre describing the building properties,
see~\cite{MunozH.2014.IJM}.

The disadvantages of using the digital cadastre for the computation of heating
demand are:
(1) the digital cadastre of other type of building information data
are not as homogeneous as demographic data and building typologies, there are
many building typologies available for Europe
\cite{Caputo.2013,Hrabovszky.2013,Kragh.2013,Singh.2013,DallO.2012,Dascalaki.2011,Balaras.2007},
as for its demographic data.
(2) the complexity of data representing the building stock makes it difficult
to make projections into the future,
\citeA{MunozH.2015.IBPSA.Pop} present an application of a synthetic
building stock projected into the future with a simplified building geometry.

\section{Simulation: Using GREGWT to Reweight the Microcensus}\label{sec:gregwt}

The aim of the GREGWT is to reweight a survey implementing method number 5
from~\citeA{SinghMohl.1996}.
\citeA{Tanton.2011a} makes a detailed description of the algorithm and its
applications.
\citeA{Rahman.2013} presents an application of the GREGWT algorithm for the
construction of synthetic population, the authors show a good performance of
the method for the estimation of household stress in Australia.
The mathematical description of the GREGWT algorithm presented below is taken
from~\citeA{Rahman.2010} and the algorithm description
from~\citeA{MunozH.2015.IMA.GREGWT}.

Aim of the GREGWT algorithm is to find a set of new weights $w$ that can be
used to match a survey $X$ to a set of given benchmarks $T$ so that $T = \sum
w_{j} X_{j}$ (e.g.\ small area aggregates) by minimizing the
weight difference between these new weights $w$ and the sample design weights
$d$ from the survey. For the distance $D$ between design and estimated weights
the GREGWT algorithm makes use of the truncated Chi-Squared distance function,
represented in Equation~\ref{eq:distance}.

\begin{equation} \label{eq:distance}
    D = \frac{1}{2} \sum_j \frac{{\left(w_j - d_j \right)}^2}{d_j}
\end{equation}

The equation needed to minimize the weight distance constraint to some given
marginal totals of a geographical area ($T$) can be expressed as the
Lagrangian function of the Chi-Squared function, as follows:

\begin{equation} \label{eq:L}
    L = \frac{1}{2} \sum_j \frac{{\left(w_j - d_j \right)}^2}{d_j} +
    \sum_{k} \lambda_k \left(T_{k} - \sum_j  w_{j,k} X_{j,k} \right)
\end{equation}

By differentiating (\ref{eq:distance}) with respect to $w_j$ and applying the
first order condition, we have:

\begin{equation}
    \frac{\delta L}{\delta w_j} = \left( \frac{w_j - d_j}{d_j} \right) -
    \sum_{j} \lambda_j X_j = 0
\end{equation}

With this equation we can formulate an equation for the new weights.
Where $X^{'}_j =  \sum \lambda_k X_{j,k}$.

\begin{equation}
    w_j = d_j + d_j X^{'}_j
\end{equation}

The new weights computed by the GREGWT algorithm are float values. Without any
restrictions the algorithm will produce negative weights, both implementations
of the algorithm introduce boundary constrains as user input. The user can
define an upper and lower bound, if the algorithm computes weights outside
these bounds the weights will be truncated to the corresponding bounds. In this
case the algorithm will iterate with the new computed weights until a predefined
convergence parameter is met or until there is no improvement in the iteration.

The implementation of the GREGWT algorithm in the R language adds an extra
calibration method for the new estimated weights. This last process makes sure
that the sum of the new weights is equal to the total population of the
specific area. Equation~\ref{eq:align} shows the alignment used to calibrate
the resulting weights.

\begin{equation}\label{eq:align}
    wo_i = \frac{p\times w_i}{\sum{w_i}}
\end{equation}

Where $wo$ are the new calibrated weights, $p$ is the population total and
$w$ are the computed new weights from the GREGWT algorithm.

\section{Benchmarking to Different Aggregation Units}\label{sec:bench}

The defined benchmarks for the reweighting of the survey are aggregated by
different units:
(1) Individuals,
(2) Families, and
(3) Buildings
(see Table~\ref{tab:census} and Section~\ref{sec:data}).
The used R implementation of GREGWT algorithm is able to perform an integrated
reweight.
This is important for maintaining unit aggregations, f.ex.\ to maintain the
family structure given by the survey.
Nonetheless, this does not allow us to benchmark to different aggregation
units.
Attempts to address this issue exist in the literature,
\citeA{Guo.2007} benchmark an initial survey to household characteristics and
fit the result to individual benchmarks in the
integerization\footnote{%
    This term is used by the spatial microsimulation community to described
    the process of converting resulting fraction weights to integer weights.
}
of the weights via a Monte Carlo process.
This method allows to ``benchmark'' to both aggregation units.
The disadvantage of this approach relies on its integerization process, this
process is only required for the
construction of agents at the cost of a decline of the algorithm performance.
For the presented simulation we do not require individual agents and therefore
any form of integerization would be contra productive for the final result.
For a description of other similar approaches see
\citeA{Pritchard.2012} and \citeA{Ma.2015}.
\citeA{Pritchard.2012} propose a ``Conditional Monte Carlo Synthesis
Procedure'' to fit the synthetic population to both: household and individual
benchmarks.
Again this procedure assumes an integerization of the survey.
As described above we aim to develop a method that does not require an
integerization step in order to avoid:
(a) a decline in the performance of the algorithm,
(b) an explosion of computational time, and
(c) introduction of a stochastic element to a deterministic approach.
\citeA{Ma.2015} developed another method to create a synthetic population:
``fitness-based synthesis'' (FBS).
The method presented by \citeauthor{Ma.2015} proposed
the computation of two fitness values expressing the adding and subtracting
probability of individuals from the random selected population from
the reweighted population survey.

The GREGWT algorithm needs to transform the input matrix $X$ (micro census) to a
binary matrix, each 1 on the matrix corresponds to an individual. This works
well if we benchmark the survey to aggregates counting individuals (e.g.\
number of individuals on age category 18 to 20) but fails if we try to
benchmark to an aggregate counting dwelling units (e.g.\ number of dwelling
units with floor space of 60 $m^2$). We need to control for this difference.
With the available information of the input survey we can manipulate the binary
matrix in order to make the benchmarking to different aggregation units
possible.
For variables benchmarked to dwelling units we divide them by
household size, an individual with household size 3 will get a 1/3 instead of 1
in the $X$ input matrix. For all variables counting buildings we divide the
values by the household size and number of dwelling units, the same individual
living in a building with 6 dwelling units will have a value of 1/3/6. This
calibration is formalized in Equation~\ref{eq:du}~and~\ref{eq:building}, where
$HH$ is household size and $DU$ is number of dwelling units.

\begin{equation}\label{eq:du}
    X_i^{du} = X_i \div HH_i
\end{equation}

\begin{equation}\label{eq:building}
    X_i^{bu} = X_i \div HH_i \div DU_i
\end{equation}

With this simple modification of the survey we are able to benchmark the input
survey to three aggregation levels. In theory this method allows us to
benchmark to any number of aggregation units.

\section{Results: Heating Demand}\label{sec:results}

In this section we present the main results from the performed spatial
microsimulation. The results show the estimated heating demand for the German
residential sector. First we present an internal validation of the spatial
microsimulation model and the resulting heating demand. The results are aggregated
back to the geographical areas in order to visualize them.

In order to internally validate the model we compare the results at the NUTS--3
level.
We compare the output results with the benchmarks used in the reweighting
process. For the comparison we make use of the Total Absolute Error $(TAE)$ and
the Percentage Absolute Error $(PTAE)$, two measures of the model internal
error.
The $TAE$ is the absolute difference between the simulated
$\hat{T}$ and observed $T$ benchmarks, the $PTAE$ is an extension of the
$TAE$ measure.
The $PTAE$ divides the computed $TAE$ by the total population $pop$ of the
geographical area $i$.
The mathematical expressions of both measures are expressed below.

\begin{equation}
    TAE_i = \sum_i \left| T_i - \hat{T}_i \right|
    \label{eq:tae}
\end{equation}

\begin{equation}
    PTAE_i = TAE_i \div pop_i \times 100
    \label{eq:ptae}
\end{equation}

\input{FIGURES/tae}

The resulting $PTAE$ values show a very low miss-allocation of individuals at
this aggregation level.
There are only four areas with a $PTAE$ value higher than 0.2\% and 52 areas
with a value higher than 0.1\%.
Figure~\ref{fig:tae} shows:
(a) the distribution of the $PTAE$ values for all simulation areas; and
(b) a scatter plot comparing the observed and simulated marginal sums.
Figure~\ref{fig:tae}b compares the simulated and observed small area
benchmarks.
The plot shows a very good performance of the estimation.
The mean $TAE$ of the estimation of 182.05, this means that on average 182.05
individuals are misrepresented on a small area.
In order to compared the misrepresentation of individuals per small are we
divide this value by the total population on the corresponding area $PTAE$.
The mean $PTAE$ value is of 0.04.
This means that on average 0.04\% of individuals are misrepresented on the
small areas.

The Internal validation of the model shows extremely good results, these
results do not reflect on the accuracy of the estimated heating demand.
An external validation of the model is necessary in order to validate the
estimation of heating demand.
This paper shows that the use of a spatial microsimulation model for the
estimation of heating demand at low aggregation levels is possible.
The underlying model used for the estimation of heating demand has not yet been
validated, still the estimation is able to show plausible differences in
heating demand between small geographical areas.
This method can be applied at a city level, differentiating the heating demand
at a neighbourhood level.

An external validation of the model is not possible because energy consumption
data is not available at this disaggregation level.
Available energy consumption data exist at a higher aggregation level.
This data does not differentiate between consumption sectors, neither does it
between primary and end-energy consumption.
Our model simulates residential end-heating demand.
There is also a difference between heating demand and heating consumption, our
model does not consider efficiency rates of heating supply infrastructure.
Further steps are planned to make an external validation possible.
The first step we envision towards a validation of estimated heating demand is
the integration of the non residential sector to the model.
With an estimate of the non residential sector we might be able to validate the
sum of residential and non residential heating demand at a higher aggregation
level.
We also plan to include efficiency rates of the underlying heating supply
infrastructure as well as the energy carrier used to supply the heating.

\input{FIGURES/duheat}

In order to show the results in a more meaningful way we divide the estimated
total heating demand by the number of dwelling units in each geographical area and
by a constant (60) representing the average dwelling unit size in $m^2$ for
Germany\footnote{71,5 $m^2$ (2013 west Germany) \& 63.4 $m^2$ (2013
east Germany) from: Income and Consumption survey ``Einkommens- und
Verbrauchsstichprobe'' (EVS) as quoted in:
``Haushalte zur Miete und im Wohneigentum nach Anteilen und Wohnfläche in den
Gebietsständen am 1.1.'' destatis.de}.
The resulting specific heating demand allows us to asses the performance of the
model. The distribution of this value for all simulation areas is plotted on
Figure~\ref{fig:heat-du}, the values and distribution shown in this plot are
consistent to known consumption values for the German building stock.

\input{FIGURES/heat}
\input{FIGURES/berlin}

We present two figures showing the estimated heating demand at a NUTS-3 level. The
first map (Figure~\ref{fig:germany}) shows the distribution of specific heating
demand for the entire country. The second map shows the detail of four specific
areas of Germany.
In addition to the estimated heating demand we plot urban agglomerations retrieved
from Landsat images, we make use of the GADM dataset containing this
information~\cite{Hijmans.2014}.

On the first map (see Figure~\ref{fig:germany}) we can identify a
differentiation between West- and East-Germany.
The specific heating consumption is higher in East-Germany.
The specific heating consumption on East-Germany is higher because the building
stock on this part of Germany is older and less energy efficient.
This regional differentiation on heating demand shows that the presented model
is sensitive to regional characteristics of the building stock.
There is a big difference between East and West regarding the heating supply
infrastructure.
The share of households connected to a district heating network in East-Germany
is much higher than in West-Germany.
Also the construction quality of the building stock differs significantly
between these regions.
This clear differentiation between small areas shows that the model is able to
correctly capture the differences of the building stock.

The second map (Figure~\ref{fig:berlin}), showing a detail of the distribution
of heating demand for the two largest cities in Germany:
(a) Berlin and its surroundings,
(b) Hamburg and its surroundings and two urban agglomerations:
(c) the south-east part of North Rhine-Westphalia ``Nordrhein Westfalen'' and
(d) Thuringia ``Th\"uringen'', a federal state of East-Germany.
In the case of the German cities, Berlin and Hamburg, the official NUTS--3
level is equivalent to the NUTS--2 and NUTS--1 level.
Both areas have a disproportional large population size compared to all the
other geographical areas.
Statistics at a lower aggregation level are available for both cities and a
second reweighting algorithm could be perform for these two areas separately.
In both cases we see that the areas corresponding to the cities have a higher
specific heating demand and the peripheries show a lower specific heating demand, we
find an explanation for this differentiation in the construction year of the
corresponding building stock. We expect to have an old building stock within
the historical urban fabric of the cities, this part of the city will be well
within the corresponding geographical area. Both cities have outgrown the
boundaries of these geographical areas. The development of urban settlements at
the periphery of the city, constructed in more recent years, are constrained to
newer building codes regulating the heating transmission losses of the building
shell and therefore consuming less heat.

We see a similar effect on the large urban agglomeration of North
Rhine-Westphalia (see Figure~\ref{fig:berlin}c), the high values of heating demand
correspond to the original settlements in the region. These settlements have
the oldest building stock in the region. Large part of these settlements are
under heritage protection, making the retrofit of the buildings extremely
expensive and difficult.
We can't see this differentiation on the East-German urban agglomeration in the
Thuringia state (see Figure~\ref{fig:germany}d). The urban agglomeration is not
as big as in North Rhine-Westphalia and the urban and population growth hasn't
been as big as in other German regions.
In the case of North Rhine-Westphalia we see the opposite phenomena in which
more rural areas in the periphery have higher consumption values.

The results presented in this paper show that an estimation of heating demand at
this level of aggregation with relative little input data is possible.
Future developments of the models aim to integrate projections of the heating
demand consumption at the same area of aggregation. A theoretical background to
perform a projection of heating demand with a spatial microsimulation model
exists~\cite{MunozH.2015.IBPSA.Pop}. \citeA{MunozH.2015.IBPSA.Pop} project the
heating demand at a low aggregation level for the city of Hamburg using a spatial
microsimulation model. Recent developments show an alternative method to
project heating demand by simulating retrofits through the manipulation of the
input weights of the micro census~\cite{MunozH.2015.IMA.GREGWT}. This method
presents a clear advantage while simulating and projecting heating demand for the
entire country at a low level of aggregation because we do not have to make
assumptions for the projection of retrofit levels at a low level of
aggregation. A possible implementation of this method is to align the initial
weight distribution to proposed policies implemented at a federal level. In
this scenario the aggregated retrofit rates are defined at a federal level and
the allocation of these retrofits could be simulated as function of demographic
characteristics at a lower geographical level.

\section{Next Steps: Integrated Reweighting}\label{sec:next_steps}

The next envisioned step is to group the synthetic individuals into:
(a) households and
(b) into buildings.
The advantage of grouping individuals into single buildings are:
(1) the ability to represent energy consumption at a building level and
(2) the possibility to geo-reference these buildings at a finer spatial resolution.

One of the applications for this synthetic data is the dimensioning and
planning of decentralized energy supply systems.
In order to make data usable for this application we need to represent energy
demand at a finer spatial resolution.
We may achieve this by obtaining aggregated benchmarks at a finer spatial
resolution, this may not be possible due to data protection concerns.
For this reason we want to develop new methods to further disaggregate heating
consumption values.
For this disaggregation we see two possible paths, the first step for both
paths is the grouping of individual data into buildings.

The first approach makes use of the digital cadastre.
The problem of this method is that a digital cadastre may not always be able
for the desire area, this is specially true for rural areas.
The second approach makes use of satellite images for the computation of
spatial probability distributions of relevant building parameters (e.g.\
density, construction year, etc.) and stochastically allocated buildings to
build up areas based on the computed spatial probabilities.
The advantage of this method is that satellite images are available for the
entire world, making the transferability of the method higher.

With a more efficient building stock in place the roll of user-behaviour will
take a dominant place in the estimation of heating demand~\cite{Hong.2015}.
The presented method already solves a common problem of urban models aimed
at the estimation of residential heating demand with an explicit consideration
of human behaviour, this is the allocation of families to the building stock.
\citeA{MunozH.2014.IJM} use a spatial microsimulation model to describe
households and the building characteristics they reside on for the estimation
of heating demand varying user related parameters on the model according to the
demographics of the user.
A more elaborated approach is presented by~\citeA{MunozH.2014.IBPSA-JP}, in
this paper the author enriches the German micro census with a time-use survey
for the generation of household schedules, these schedules are used as input in
a thermal simulation model.

\section{Conclusions: Creating Rich Data-Sets for the Urban Simulation Community}\label{sec:conclusions}

The presented method describes a process to represent core characteristics of
the building stock in an abstract fashion without fully describing, neither the
real building geometry not an accurate location of the individual buildings.
%
We make use of a new R library~\cite{MunozH.2015.GREGWTR} for the reweighting
of the German micro census survey (2010) to small geographical areas,
corresponding to the European NUTS-3 level.
Available aggregated data regarding demographic information as well as
information on the building stock are available at this level from the last
German census (2011).
%
In order estimate heat demand at a micro level we classify each individual in
the micro census to a building type. We make use of a well established building
typology for this classification~\cite{Loga.2011}. Because we know the dwelling
unit size of every individual in the micro census we are able to transform the
specific heat demand provided by the building typology into an absolute value,
we control this value by household size, dividing the absolute estimate heat
demand by the household size of each individual.

We make use of an innovative feature of the R library which allows us to
benchmark the micro census to three different levels of aggregation: (1)
individuals, (2) households and (3) building units.
%
Preliminary results from the simulation are presented as maps at the NUTS-3
level. On these results we are able to identify a distinction between East and
West Germany as well as a distinction between historical urban agglomerations,
with a high heat demand, and new developments in the suburbs, with a more
moderate heat demand.
%
We conclude this paper by offering the reader a prospective on the challenges
ahead and the potential of the application of this method for other kind of
urban related phenomena.

A construction of synthetic cities by means of a spatial microsimulation model
seems promising, the generation of such a rich data-set with a relative low
amount of input data is extremely useful, specially for regions without detailed
information about their building stock.
In addition to a simplified representation of the building stock this model
connects the households populating the building stock, increasing the usability
of this synthetic data to an entire new community,
we are specially interested in the emerging community exploring the nexus
between health and indoor environment of the building stock.

\newpage

\renewcommand{\refname}{REFERENCES} %
\addcontentsline{toc}{section}{REFERENCES}
\bibliographystyle{apacite}
\bibliography{mybib.bib}

\addcontentsline{toc}{section}{Notes}

\AtEndDocument{%
    \begingroup
    \def\enoteheading{
        \vspace{4em}
        \rule{6cm}{.1pt}        \vspace{1em}
    }\label{TheEnd}
    \endgroup
}

\newpage
\section*{Annex}
\input{TABLES/categories}% \ref{tab:census-cat}
\input{TABLES/rules}

\end{document}
